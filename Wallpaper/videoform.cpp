#include "videoform.h"
#include "ui_videoform.h"
#include <QMediaPlaylist>
#include <QVideoWidget>
#include <QMediaPlayer>

VideoForm::VideoForm(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::VideoForm)
{
    ui->setupUi(this);
}

VideoForm::~VideoForm()
{
    delete ui;
}
void VideoForm::load(QString path){
    VideoForm::player.setMedia(QMediaContent(QUrl::fromLocalFile(path)));
}
void VideoForm::play(){
    VideoForm::player.play();
}
void VideoForm::pause(){
    VideoForm::player.pause();
}
