#ifndef WIDGET_H
#define WIDGET_H

#include <QVideoWidget>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QSystemTrayIcon>

#include "about.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    About about;
    QVideoWidget videoWidget;
    QMediaPlayer player;
    QSystemTrayIcon tray;
    QMediaPlaylist *list;
    QAction *action_showDialog;
    QAction *action_play;
    QAction *action_exit;

private slots:
    void addMenu();

    void on_openButton_clicked();

    void on_playButton_clicked();

    void on_exitButton_clicked();

    void on_aboutButton_clicked();

    void OnSystemTrayClicked(QSystemTrayIcon::ActivationReason);

    void LoadFile(const QString file);

    void ReadSetting();

    void WriteSetting();

    void on_checkBox_1_clicked();

    void AutoRun(bool flag);

    void on_volumeSlider_valueChanged(int value);

    void on_volumeSlider_sliderReleased();

    void onShowDialogClick();

    void on_rateSlider_sliderReleased();

    void on_rateSlider_valueChanged(int value);

    void on_rateSlider_sliderPressed();

    bool eventFilter(QObject *obj,QEvent *event);

public slots:

    static void ReFreshBackground();

    void setAutoRun(bool isAutoRun);

private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
