#include <windows.h>
#include <qsettings.h>
#include <qmessagebox.h>
#include <qclipboard.h>
#include <qdesktopservices.h>
#include "about.h"
#include "ui_about.h"
#include "support.h"
#include "widget.h"

Widget *father;

About::About(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
}

About::~About()
{
    delete ui;
}

void About::SetFather(QWidget *f){
    father = (Widget*)f;
}

void About::on_open_clicked()
{
    QDesktopServices::openUrl(QString("https://www.dearxuan.top"));
}

void About::on_refresh_clicked()
{
    Widget::ReFreshBackground();
}

void About::on_clear_clicked()
{
    QString appPath = QApplication::applicationFilePath();
    appPath = appPath.replace("/","\\");
    QSettings *reg=new QSettings(
                    HKEY_AUTORUN,
                    QSettings::NativeFormat);
    reg->remove(HKEY_NAME);
    if(reg->value(HKEY_NAME).toString() == ""){
        father->setAutoRun(false);
    }else{
        QMessageBox::warning(this,"拒绝访问","清理注册表失败.",QMessageBox::Ok,QMessageBox::Ok);
    }
    reg->deleteLater();
}

void About::on_copy_clicked()
{
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText("mail@dearxuan.top");
}

void About::on_opensource_clicked()
{
    QDesktopServices::openUrl(QString("https://blog.csdn.net/qq_39200794/article/details/119255190"));
}
