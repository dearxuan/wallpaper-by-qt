#ifndef SUPPORT_H
#define SUPPORT_H

#define PLAYER_FILTER "视频(*.mp4 *.avi *.wmv);;图片(*.jpg *.png *.bmp);;所有文件(*.*)"
#define BUTTON_TEXT_PLAY "播放"
#define BUTTON_TEXT_PAUSE "暂停"

#define HKEY_NAME "Wallpaper_By_DearXuan"
#define HKEY_AUTORUN "HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run"
#define HKEY_PARAMETER " -autorun"

#define OPEN_REFRESHBAKGROUND_FILE_PATH "ReFreshBackground.exe"

#define VOLUME_HIGH "image: url(:/Image/volume-high.png);"
#define VOLUME_LOW "image: url(:/Image/volume-low.png);"
#define VOLUME_OFF "image: url(:/Image/volume-off.png);"

#define SHARED_MEMORY_KEY "wallpaper_by_dearxuan"

#endif // SUPPORT_H
