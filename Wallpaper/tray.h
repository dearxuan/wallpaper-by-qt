#ifndef TRAY_H
#define TRAY_H

#include <QDialog>
#include <QtGui>
#include <QSystemTrayIcon>

class Tray:public QDialog
{
    Q_OBJECT
public:
    explicit Tray(QWidget *parent = nullptr);
    void CreatTrayMenu();
    void CreatTrayIcon();
    QSystemTrayIcon *myTrayIcon;
    QMenu *myMenu;
    QAction *miniSizeAction;
    QAction *maxSizeAction;
    QAction *restoreWinAction;
    QAction *quitAction;

protected:
    void tray_click();

signals:

public:slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
};

#endif // TRAY_H
