#ifndef ABOUT_H
#define ABOUT_H

#include <QWidget>

namespace Ui {
class About;
}

class About : public QWidget
{
    Q_OBJECT

public:
    explicit About(QWidget *parent = nullptr);
    ~About();

    Ui::About *ui;

    void SetFather(QWidget *f);

private slots:
    void on_open_clicked();
    void on_refresh_clicked();
    void on_clear_clicked();
    void on_copy_clicked();
    void on_opensource_clicked();
};

#endif // ABOUT_H
