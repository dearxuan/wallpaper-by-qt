#include <QApplication>
#include <QMessageBox>
#include <windows.h>
#include "widget.h"

bool isOpenMore();

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    if(isOpenMore()){
        qApp->quit();
        //QMessageBox::warning(NULL,"错误","程序正在运行");
        return 0;
    }
    Widget w;
    if(!(argc >= 2 && strcmp(argv[1],"-autorun") == 0)){
        w.show();
    }
    return a.exec();
}

bool isOpenMore(){
    HANDLE hMutex = CreateMutex(NULL,true,TEXT("wallpaper_by_dearxuan"));
    return GetLastError() == ERROR_ALREADY_EXISTS;
}
