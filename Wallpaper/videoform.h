#ifndef VIDEOFORM_H
#define VIDEOFORM_H

#include <QWidget>
#include <QMainWindow>
#include <QMediaPlaylist>
#include <QVideoWidget>
#include <QMediaPlayer>

namespace Ui {
class VideoForm;
}

class VideoForm : public QWidget
{
    Q_OBJECT

public:
    explicit VideoForm(QWidget *parent = nullptr);
    ~VideoForm();
    QMediaPlayer player;
    void load(QString path);
    void play();
    void pause();

private:
    Ui::VideoForm *ui;
};

#endif // VIDEOFORM_H
