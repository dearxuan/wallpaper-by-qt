## Wallpaper By Qt
使用QT开发的桌面动态壁纸软件

## 原理
[使用C++实现的仿Wallpaper动态壁纸软件(含源文件)——DearXuan的主页](https://www.dearxuan.top/2021/07/28/%E4%BD%BF%E7%94%A8C-%E5%AE%9E%E7%8E%B0%E7%9A%84%E4%BB%BFWallpaper%E5%8A%A8%E6%80%81%E5%A3%81%E7%BA%B8%E8%BD%AF%E4%BB%B6/)

## 界面
![截屏](https://gitee.com/dearxuan/picture/raw/master/image/Wallpaper_Qt_0.jpg)

## 环境
Qt版本: Qt Creator 4.11.1 Community<br/>
系统版本: Windows10 19043

## 克隆
```bash
git clone https://gitee.com/dearxuan/wallpaper-by-qt.git
```